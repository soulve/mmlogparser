﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMLogParser
{
    public static class ExtendedListOperations
    {
        public static decimal GetMedian(this IEnumerable<long> source)
        {
            // Create a copy of the input, and sort the copy
            long[] temp = source.ToArray();
            Array.Sort(temp);

            int count = temp.Length;
            if (count == 0)
            {
                throw new InvalidOperationException("Empty collection");
            }
            else if (count % 2 == 0)
            {
                // count is even, average two middle elements
                long a = temp[count / 2 - 1];
                long b = temp[count / 2];
                return (a + b) / 2m;
            }
            else
            {
                // count is odd, return the middle element
                return temp[count / 2];
            }
        }

        public static long Variance(this IEnumerable<long> source)
        {
            var count = source.Count();

            if (count == 0) return 0;

            var mean = source.Average();
            var squaredDiffs = source.Select(d => (d - mean) * (d - mean));
            return Convert.ToInt64(squaredDiffs.Average());
        }

        public static long StdDev(this IEnumerable<long> source)
        {
            return Convert.ToInt64(Math.Sqrt(source.Variance()));
        }

        public static long AveragePercentage(this IEnumerable<long> source, int percentage)
        {
            var count = source.Count();

            if (count == 0) return 0;

            if (count == 1) return source.First();

            return Convert.ToInt32(source.OrderBy(s => s).Take(Convert.ToInt32((count * percentage) / 100)).Average());
        }
    }
}
