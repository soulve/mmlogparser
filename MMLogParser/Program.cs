﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMLogParser
{
    class Program
    {
        private static readonly string baseLocation = @"C:\Users\ronald\Downloads\medimapppackage\iislogs\";

        private static readonly string customer = "radboudumc";

        private static readonly bool details = false;

        static void Main(string[] args)
        {
            string[] fileEntries = Directory.GetFiles(Path.Combine(baseLocation, customer));

            var events = new List<Event>();

            int date = 0;
            int time = 1;
            int sIp = 2; 
            int csMethod = 3;
            int csUriStem = 4;
            int csUriQuery = 5;
            int sPort = 6;
            int csUsername = 7;
            int cIp = 8;
            int csUserAgent = 9;
            int csReferer = 10;
            int scStatus = 11;
            int scSubstatus = 12;
            int scWin32Status = 13;
            int timeTaken = 14;
            foreach (string logFile in fileEntries)
            {
                if (logFile.EndsWith(".log"))
                {
                    var lines = File.ReadLines(logFile);
                    foreach (var line in lines)
                    {
                        if (line.StartsWith("#Fields:"))
                        {
                            if (line.ToLower().IndexOf("s-sitename s-computername") > -1)
                            {
                                date = 0;
                                time = 1;
                                sIp = 4;
                                csMethod = 5;
                                csUriStem = 6;
                                csUriQuery = 7;
                                sPort = 8;
                                csUsername = 9;
                                cIp = 10;
                                csUserAgent = 12;
                                csReferer = 14;
                                scStatus = 16;
                                scSubstatus = 17;
                                scWin32Status = 18;
                                timeTaken = 21;
                            }
                            else
                            {
                                date = 0;
                                time = 1;
                                sIp = 2;
                                csMethod = 3;
                                csUriStem = 4;
                                csUriQuery = 5;
                                sPort = 6;
                                csUsername = 7;
                                cIp = 8;
                                csUserAgent = 9;
                                csReferer = 10;
                                scStatus = 11;
                                scSubstatus = 12;
                                scWin32Status = 13;
                                timeTaken = 14;
                            }
                        }
                        else if (!line.StartsWith("#"))
                        {
                            //# Fields: date time                           s-ip cs-method cs-uri-stem cs-uri-query s-port cs-username c-ip            cs(User-Agent)            cs(Referer)         sc-status sc-substatus sc-win32-status                   time-taken
                            //# Fields: date time s-sitename s-computername s-ip cs-method cs-uri-stem cs-uri-query s-port cs-username c-ip cs-version cs(User-Agent) cs(Cookie) cs(Referer) cs-host sc-status sc-substatus sc-win32-status sc-bytes cs-bytes time-taken
                            var variables = line.Split(' ');
                            events.Add(new Event {
                                DateTime = GetDateTime(variables[date], variables[time]),
                                ServerIp = variables[sIp],
                                Method = variables[csMethod],
                                Uri = variables[csUriStem],
                                MMUri = GetMMUri(variables[csUriStem]),
                                Query = variables[csUriQuery],
                                Port = GetInt(variables[sPort]),
                                Username = variables[csUsername],
                                ClientIp = variables[cIp],
                                UserAgent = variables[csUserAgent],
                                Referer = variables[csReferer],
                                Status = GetInt(variables[scStatus]),
                                SubStatus = GetInt(variables[scSubstatus]),
                                WinStatus = GetLong(variables[scWin32Status]),
                                TimeTaken = GetLong(variables[timeTaken]),
                                IsAdmin = IsAdmin(variables[csUriStem]),
                                IsBroker = IsBroker(variables[csUriStem])
                            });
                        }
                    }
                }
            }

            // only 200 and only MM
            events = events.Where(e => e.Status == 200 && !e.IsAdmin && !e.IsBroker).ToList();

            if (details)
            {
                // details per request
                var perUri = events.GroupBy(e => e.MMUri);
                WriteLogs(events, perUri);
            }
            else
            {
                // overview per month
                var perMonth = events.GroupBy(e => e.DateTime.Year + "-" + e.DateTime.Month);
                WriteLogs(events, perMonth);
            }
        }

        private static void WriteLogs(List<Event> totalEvents, IEnumerable<IGrouping<string, Event>> groupedEvents)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(baseLocation + "result.csv"))
            {
                if (groupedEvents.Count() > 0)
                {
                    file.WriteLine("\"URL\";Aantal;Gemiddelde;90%;Min;Max;Mediaan");
                    file.WriteLine("\"Overall\";" + 
                        totalEvents.Count() + ";" + 
                        Convert.ToInt32(totalEvents.Average(e => e.TimeTaken)) + ";" +
                        totalEvents.Select(e => e.TimeTaken).AveragePercentage(90) + ";" +
                        totalEvents.Min(e => e.TimeTaken) + ";" + 
                        totalEvents.Max(e => e.TimeTaken) + ";" + 
                        Convert.ToInt32(totalEvents.Select(e => e.TimeTaken).GetMedian()));
                    foreach (var events in groupedEvents)
                    {
                        file.WriteLine("\"" + events.Key + "\";" + 
                            events.Count() + ";" + 
                            Convert.ToInt32(events.Average(e => e.TimeTaken)) + ";" +
                            events.Select(e => e.TimeTaken).AveragePercentage(90) + ";" +
                            events.Min(e => e.TimeTaken) + ";" + 
                            events.Max(e => e.TimeTaken) + ";" + 
                            Convert.ToInt32(events.Select(e => e.TimeTaken).GetMedian()));
                    }
                }
            }
        }

        private static bool IsAdmin(string uri)
        {
            return uri.ToLower().IndexOf("medimappadmin") > -1 || uri.ToLower().IndexOf("hangfire") > -1 || uri.ToLower().IndexOf("/admin/") > -1 || uri.ToLower().IndexOf("elmah") > -1;
        }

        private static bool IsBroker(string uri)
        {
            return uri.ToLower().IndexOf("medimappepicservices") > -1 || uri.ToLower().IndexOf("medimappepdservices") > -1;
        }

        private static string GetMMUri(string uri)
        {
            var mmUri = "";
            if (uri.ToLower().IndexOf("/content/uploads/") > -1)
            {
                mmUri = "/Content/uploads/";
            }
            else if (uri.ToLower().IndexOf("/content/img/") > -1)
            {
                mmUri = "/Content/img/";
            }
            else if (uri.ToLower().IndexOf("/content/js/") > -1)
            {
                mmUri = "/Content/js/";
            }
            else if (uri.ToLower().IndexOf("/content/vendor/") > -1)
            {
                mmUri = "/Content/vendor/";
            }
            else if (uri.ToLower().IndexOf("/content/css/") > -1)
            {
                mmUri = "/Content/css/";
            }
            else if (uri.LastIndexOf("/") > 5)
            {
                var lastPart = uri.Substring(uri.LastIndexOf("/") + 1, uri.Length - uri.LastIndexOf("/") - 1);

                int n;
                bool isNumeric = int.TryParse(lastPart, out n);
                if (isNumeric)
                {
                    mmUri = uri.Substring(0, uri.LastIndexOf("/"));
                }
                else
                {
                    mmUri = uri;
                }
            }
            else
            {
                mmUri = uri;
            }

            return mmUri;
        }

        private static int GetInt(string v)
        {
            var i = 0;

            if (!string.IsNullOrEmpty(v) && v != "-")
            {
                i = int.Parse(v);
            }

            return i;
        }

        private static long GetLong(string v)
        {
            var i = 0l;

            if (!string.IsNullOrEmpty(v) && v != "-")
            {
                i = long.Parse(v);
            }

            return i;
        }

        private static DateTime GetDateTime(string date, string time)
        {
            return DateTime.ParseExact(date + " " +  time, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
        }
    }

    public class Event
    {
        public DateTime DateTime { get; set; }

        public string ServerIp { get; set; }

        public string Method { get; set; }

        public string MMUri { get; set; }

        public string Uri { get; set; }

        public string Query { get; set; }

        public int Port { get; set; }

        public string Username { get; set; }

        public string ClientIp { get; set; }

        public string UserAgent { get; set; }

        public string Referer { get; set; }

        public int Status { get; set; }

        public int SubStatus { get; set; }

        public long WinStatus { get; set; }

        public long TimeTaken { get; set; }

        public bool IsAdmin { get; set; }

        public bool IsBroker { get; set; }
    }
}
